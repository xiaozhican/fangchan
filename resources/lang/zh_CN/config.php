<?php
return [
    'labels' => [
        'Config' => 'Config',
        'config' => 'Config',
    ],
    'fields' => [
        'key' => 'key',
        'name' => 'name',
        'value' => 'value',
    ],
    'options' => [
    ],
];
