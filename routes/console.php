<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('test', function () {
   Cache::put(1, 19674);
   Cache::put(2, 19677);
   Cache::put(3, 19649);
   Cache::put(4, 19661);
   Cache::put(5, 19766);
   Cache::put(6, 19692);
   Cache::put(9, 19651);
   Cache::put(10, 19663);
   Cache::put(11, 19686);
   Cache::put(12, 19653);
   Cache::put(13, 19650);
   Cache::put(14, 19757);
});
