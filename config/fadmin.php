<?php

return [
    'postUrl' => env('POST_URL'),
    'homePath' => env('HOME_PATH'),
    'picPath' => env('PIC_PATH'),
    'categories' => [
        1 => [
            'name' => '厦门',
            'pinyin' => 'https://xm.loupan.com/xinfang',
        ],
//        2 => [
//            'name' => '室外设施',
//            'pinyin' => 'https://fangdichan.huangye88.com/shiwaisheshi/',
//        ],
        /*
        3 => [
            'name' => '未解之谜',
            'pinyin' => 'weijiezhimi',
        ],
        4 => [
            'name' => '世界之最',
            'pinyin' => 'shijiezhizui',
        ],
        5 => [
            'name' => '历史趣事',
            'pinyin' => 'lishizhenxiang',
        ],
        6 => [
            'name' => '宇宙奥秘',
            'pinyin' => 'waixingaomi',
        ],
        9 => [
            'name' => '奇趣自然',
            'pinyin' => 'qiquziran',
        ],
        10 => [
            'name' => '灵异恐怖',
            'pinyin' => 'lingyikongbu',
        ],
        11 => [
            'name' => '科学探索',
            'pinyin' => 'kexuetansuo',
        ],
        12 => [
            'name' => '猎奇怪事',
            'pinyin' => 'lieqiguaishi',
        ],
        13 => [
            'name' => '野史传说',
            'pinyin' => 'yeshichuanshuo',
        ],
        14 => [
            'name' => '民间习俗',
            'pinyin' => 'minjianxisu',
        ],
        */
    ],
];
