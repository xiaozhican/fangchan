<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;

Admin::routes();

Route::group([
    'prefix'     => config('fadmin.route.prefix'),
    'namespace'  => config('fadmin.route.namespace'),
    'middleware' => config('fadmin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('configs', 'ConfigController');
});
