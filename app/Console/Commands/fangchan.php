<?php

namespace App\Console\Commands;

use App\Models\Config;
use App\Models\Sell;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Nesk\Puphpeteer\Puppeteer;
use Overtrue\Pinyin\Pinyin;
use Symfony\Component\DomCrawler\Crawler;

class fangchan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fangchan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '爬虫';

    private Client $client;

    public array $config;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client([
            'timeout' => 86400,
        ]);
        $dbConfig = Config::all()->toArray();
        $this->config = array_merge(['xpath' => array_column($dbConfig, 'value', 'key')], config('fangchan'));
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws GuzzleException
     * @throws Exception
     */
    public function handle(): void
    {
        //$this->category("https://www.loupan.com/?all");
        $citys = DB::table('fang_city')->select('id','name','url')->where('type',1)->limit(1)->get();
        //var_dump($citys);
        //die;
        foreach ($citys as $city) {
            $maxId = 0;
            Log::info('城市：'.$city->name);
            $chengshi = $city->id;
            $url1 = "{$city->url}/xinfang/";
            $options = [
                'http_errors' => true,
                'force_ip_resolve' => 'v4',
                'connect_timeout' => 86400,
                'read_timeout' => 86400,
                'timeout' => 0,
            ];
            $response = $this->client->get($url1,$options);//get($url);
            $content = $response->getBody()->getContents();
//            //var_dump($content);
            //file_put_contents('./fangchan.html',$content);
            $crawler = new Crawler($content);
            //var_dump($crawler->filter('.item-turn')->children('.pagenxt')->previousAll()->text());
            //die;
            if($crawler->filter('.item-turn')->count()==0 || $crawler->filter('.item-turn')->children('.z-crt')->count()==0){
                $num = 1;
            }elseif ($crawler->filter('.item-turn i')->count()==0){
                $num = $crawler->filter('.item-turn')->children('.pagenxt')->previousAll()->text();
            }else{
                $num = $crawler->filter('.item-turn i')->nextAll()->text();
            }
            if($num>2){
                $num=2;
            }
            //var_dump($num);
            for($i=1;$i<=$num;$i++){
                Log::info('页数：'.$i);
                $url = $i==1?"{$city->url}/xinfang/":"{$city->url}/xinfang/p{$i}/";
                $list = $this->list($url);
                //$list = $this->list($category);
                if (empty($list)) {
                    echo '列表页抓取错误'.PHP_EOL;
                    continue;
                }

//            foreach ($list as $key => $item) {
//                var_dump($item['title']);
//            }
                //var_dump($list);
                if ($list) {
                    echo '，需要更新'.count($list).'篇'.PHP_EOL;
                } else {
                    echo '，没有需要更新的'.PHP_EOL;
                    continue;
                }

                foreach ($list as $key => $item) {
                    //var_dump($item['url']);
                    if($item==NULL){
                        continue;
                    }

                    $pageContent = $this->page($item['url']);
                    //var_dump($pageContent['addr']);
                    if ($pageContent === false) {
                        exit('子页面抓取错误');
                    }
                    //var_dump($pageContent['content_url']);
                    if($pageContent['content_url']==""){
                        $childrenPage['info'] = '';
                    }else{
                        if(strpos($pageContent['content_url'],'https')!==false){
                            $content_url = $pageContent['content_url'];
                        }else{
                            $content_url = $city->url.$pageContent['content_url'];
                        }
                        $childrenPage = $this->children_page($content_url);
                    }
                    //var_dump($childrenPage['info']);
                    DB::transaction(function () use ($chengshi,$item,$pageContent,$childrenPage){
                    $house_id = DB::table('fang_house')->insertGetId([
                        'city' => $chengshi,
                        'agent_id' => 0,
                        'sale_status' => 2,
                        'developer_id' => 2,
                        'broker_id' => 0,
                        'developer_name' => '',
                        'tags_id' => '19,68,69',
                        'type_id' => '',
                        'title' => $item['title'],
                        'img' => $item['thumb'],
                        'price' => $item['price'],
                        'lng' => '',
                        'lat' => '',
                        'opening_time' => time(),
                        'opening_time_memo'=>'',
                        'complate_time' => time(),
                        'complate_time_memo' => '',
                        'sale_phone' => '',
                        'is_discount' => rand(0,1),
                        'discount' => '',
                        'red_packet' => 0,
                        'address' => $item['addr'],
                        'sale_address' => $item['addr'],
                        'license_key' => '',
                        'hits' => 1,
                        'ordid'=>1000,
                        'create_time' => time(),
                        'update_time' => time(),
                        'status' => rand(1,5),
                        'unit'=> 1,
                        'ratio' => 0.0,
                        'rec_position' => 0,
                        'pano_url' => '',
                        'renovation' => 43,
                        'video' => ''
                    ]);
                    DB::table('fang_house_data')->insert([
                        'house_id' => $house_id,
                        'attr' => '{"building_type":"","property_right":"","area_covered":"","area_build":"","volume_ratio":"","greening_rate":"","plan_number":"","parking_space":"","property_type":"","property_company":"","property_fee":""}',
                        'info' => $childrenPage['info'],
                        'seo_title' => $item['title'],
                        'seo_keys' => $pageContent['title']
                    ]);
                    DB::table('fang_house_search')->insert([
                        'house_id' => $house_id,
                        'min_type' => 2,
                        'max_type' => 2,
                        'min_price' => $item['price'],
                        'max_price' => 10000,
                        'min_acreage' => $item['area'],
                        'max_acreage' => 10000
                    ]);
                    DB::table('fang_article')->insert([
                        'city' => $chengshi,
                        'house_id' => $house_id,
                        'cate_id' => rand(0,7),
                        'title' => $pageContent['title'],
                        'img' => $item['thumb'],
                        'info' => $childrenPage['info'],
                        'description' => $pageContent['content_title'],
                        'create_time' => time(),
                        'status' => 1,
                        'ordid' => 1000,
                        'update_time' => time(),
                        'editor' => 'fadmin',
                        'hits' => 0,
                        'editor_id' => 1,
                        'agent_id' => 0
                    ]);
                    DB::table('fang_position')->insert([
                        'cate_id' => 4,
                        'title' => $item['title'],
                        'house_id' => $house_id,
                        'status' => 1,
                        'ordid' => 1000,
                        'model' => 'house'
                    ]);

                    DB::table('fang_group')->insert([
                        'house_id' => $house_id,
                        'title' => $item['title'],
                        'house_title' => $item['title'],
                        'city' => $chengshi,
                        'discount' => '',
                        'old_price' => $item['price'],
                        'price' => $item['price'],
                        'sign_num' => 0,
                        'begin_time' => time(),
                        'end_time' => strtotime(date(rand(2022,2030).'-12-31 23:59:59')),
                        'img' => $item['thumb'],
                        'file' => '[]',
                        'status' => 1,
                        'ordid' => 1000,
                        'create_time' => time(),
                        'info' => $childrenPage['info'],
                        'hits' => 1
                    ]);
                    $phone = $this->randomMobile(1);
                    $xiaoqu  =array('西岸首府','碧桂园·滨海国际','怡心园','滨海国际小区','花园小区');
                    $flip =  array_flip($xiaoqu);
                    $estate_name = array_rand($flip,1);
                    //写字楼出售
                    DB::table('fang_office')->insert([
                        'city' => $chengshi,
                        'title' => $item['title'],
                        'estate_id' => 8,
                        'estate_name' => $estate_name,
                        'img' => $item['thumb'],
                        'price' => $item['price'],
                        'average_price' => $item['price'],
                        'acreage'=> $item['area'],
                        'address' => $item['addr'],
                        'floor' => rand(30,40),
                        'total_floor' => rand(10,20),
                        'property_fee' => rand(1,100).'元/㎡·月',
                        'grade' => 112,
                        'renovation' => rand(40,45),
                        'division' => $this->isFenGe(),
                        'type' => rand(100,102),
                        'lat' => 0,
                        'lng' => 0,
                        'tags' => '103,104,105,106,107,108,109,110,111',
                        'contact_name' => $this->getXingList().$this->getMingList().$this->getMingList(),
                        'contact_phone' => $phone[0],
                        'broker_id' => 0,
                        'user_type' => rand(1,2),
                        'hits' => 0,
                        'status' => 1,
                        'ordid' => 1000,
                        'create_time' => time(),
                        'update_time' => time(),
                        'rec_position' => 0,
                        'timeout' => strtotime(date(rand(2022,2030).'-12-31 23:59:59')),
                        'top_time' => 0,
                        'agent_id' => 0
                    ]);
                    //写字楼列表
                    $office_rental_id = DB::table('fang_office_rental')->insertGetId([
                        'city' => $chengshi,
                        'title' => $item['title'],
                        'estate_id' => 8,
                        'estate_name' => $estate_name,
                        'img' => $item['thumb'],
                        'price' => $item['price'],
                        'average_price' => $item['price'],
                        'acreage'=> $item['area'],
                        'address' => $item['addr'],
                        'floor' => rand(30,40),
                        'total_floor' => rand(10,20),
                        'property_fee' => rand(1,100).'元/㎡·月',
                        'grade' => 112,
                        'renovation' => rand(40,45),
                        'division' => $this->isFenGe(),
                        'type' => rand(100,102),
                        'lat' => 0,
                        'lng' => 0,
                        'tags' => '103,104,105,106,107,108,109,110,111',
                        'contact_name' => $this->getXingList().$this->getMingList().$this->getMingList(),
                        'contact_phone' => $phone[0],
                        'broker_id' => 0,
                        'user_type' => rand(1,2),
                        'hits' => 0,
                        'status' => 1,
                        'ordid' => 1000,
                        'create_time' => time(),
                        'update_time' => time(),
                        'rec_position' => 0,
                        'timeout' => strtotime(date(rand(2022,2030).'-12-31 23:59:59')),
                        'top_time' => 0,
                        'agent_id' => 0
                    ]);
                    DB::table('fang_office_rental_data')->insert([
                        'house_id' => $office_rental_id,
                        'info' => '',
                        'file' => '[]',
                        'seo_title' => $item['title'],
                        'seo_keys' => $item['title']
                    ]);
                    //商铺列表
                    $shops_rental_id = DB::table('fang_shops_rental')->insertGetId([
                        'city' => $chengshi,
                        'title' => $item['title'],
                        'estate_id' => 8,
                        'estate_name' => $estate_name,
                        'img' => $item['thumb'],
                        'price' => $item['price'],
                        'average_price' => $item['price'],
                        'acreage'=> $item['area'],
                        'address' => $item['addr'],
                        'floor' => rand(30,40),
                        'total_floor' => rand(10,20),
                        'property_fee' => rand(1,100).'元/㎡·月',
                        'industry' => '100,101,102,122,123,124,125,126,127,128,129,130',
                        'renovation' => rand(40,45),
                        'division' => $this->isFenGe(),
                        'type' => rand(118,120),
                        'lat' => 0,
                        'lng' => 0,
                        'tags' => '131,132,133,134,135,136',
                        'contact_name' => $this->getXingList().$this->getMingList().$this->getMingList(),
                        'contact_phone' => $phone[0],
                        'broker_id' => 0,
                        'user_type' => rand(1,2),
                        'hits' => 0,
                        'status' => 1,
                        'ordid' => 1000,
                        'create_time' => time(),
                        'update_time' => time(),
                        'rec_position' => 0,
                        'timeout' => strtotime(date(rand(2022,2030).'-12-31 23:59:59')),
                        'top_time' => 0,
                        'agent_id' => 0
                    ]);
                    //商铺出售
                    DB::table('fang_shops')->insert([
                        'city' => $chengshi,
                        'title' => $item['title'],
                        'estate_id' => 8,
                        'estate_name' => $estate_name,
                        'img' => $item['thumb'],
                        'price' => $item['price'],
                        'average_price' => $item['price'],
                        'acreage'=> $item['area'],
                        'address' => $item['addr'],
                        'floor' => rand(30,40),
                        'total_floor' => rand(10,20),
                        'property_fee' => rand(1,100).'元/㎡·月',
                        'industry' => '100,101,102,122,123,124,125,126,127,128,129,130',
                        'renovation' => rand(40,45),
                        'division' => $this->isFenGe(),
                        'type' => rand(118,120),
                        'lat' => 0,
                        'lng' => 0,
                        'tags' => '131,132,133,134,135,136',
                        'contact_name' => $this->getXingList().$this->getMingList().$this->getMingList(),
                        'contact_phone' => $phone[0],
                        'broker_id' => 0,
                        'user_type' => rand(1,2),
                        'hits' => 0,
                        'status' => 1,
                        'ordid' => 1000,
                        'create_time' => time(),
                        'update_time' => time(),
                        'rec_position' => 0,
                        'timeout' => strtotime(date(rand(2022,2030).'-12-31 23:59:59')),
                        'top_time' => 0,
                        'agent_id' => 0
                    ]);
                    DB::table('fang_shops_rental_data')->insert([
                        'house_id' => $shops_rental_id,
                        'mating' => '139,140,142,143',
                        'info' => $childrenPage['info'],
                        'file'=>'[]',
                        'seo_title' => $item['title'],
                        'seo_keys' => $item['title']
                    ]);
                    DB::table('fang_subscribe')->insert([
                        'user_id' => 0,
                        'broker_id' => 0,
                        'user_name' => $this->getXingList().$this->getMingList().$this->getMingList(),
                        'house_id' => $house_id,
                        'house_name' => $item['title'],
                        'type' => 1,
                        'model' => 'house',
                        'mobile' => $phone[0],
                        'create_time' => time(),
                        'status' => 1
                    ]);
                    //二手房
                    $arr = array('contact_name' => $this->getXingList().$this->getMingList().$this->getMingList(), 'contact_phone' => $phone[0]);
                    $contacts = json_encode($arr,JSON_UNESCAPED_UNICODE);
                    $second_house_id = DB::table('fang_second_house')->insertGetId([
                        'city' => $chengshi,
                        'title' => $item['title'],
                        'estate_id' => 8,
                        'estate_name' => $estate_name,
                        'img' => $item['thumb'],
                        'address' => $item['addr'],
                        'lat' => 0,
                        'lng' => 0,
                        'room'=>rand(1,6),
                        'living_room'=>rand(1,10),
                        'toilet' =>rand(1,5),
                        'price' => $item['price'],
                        'average_price' => $item['price'],
                        'acreage'=> $item['area'],
                        'floor' => rand(30,40),
                        'total_floor' => rand(3,20),
                        'orientations' =>rand(20,29),
                        'tags' => '95,96,97,98',
                        'hits' => 0,
                        'contacts' => $contacts,
                        'looked_num'=>0,
                        'house_type'=>rand(45,48),
                        'renovation'=>rand(41,44),
                        'broker_id' => 0,
                        'user_type' => rand(1,2),
                        'status' => 1,
                        'ordid' => 1000,
                        'create_time' => time(),
                        'update_time' => time(),
                        'ratio'=>0.8,
                        'rec_position'=>rand(0,1),
                        'timeout' => strtotime(date(rand(2022,2030).'-12-31 23:59:59')),
                        'top_time' => 0,
                        'agent_id' => 0,
                        'first_pay' =>rand(1,20)
                    ]);
                    DB::table('fang_second_house_data')->insert([
                        'house_id' => $second_house_id,
                        'file' => '[]',
                        'supporting' => '65,64,63,62,61,59,58,57,56,55,54',
                        'seo_title' => $item['title']
                    ]);
                    //出租房
                    $rental_id = DB::table('fang_rental')->insertGetId([
                        'city' => $chengshi,
                        'title' => $item['title'],
                        'estate_id' => 8,
                        'estate_name' => $estate_name,
                        'img' => $item['thumb'],
                        'address' => $item['addr'],
                        'lat' => 0,
                        'lng' => 0,
                        'room'=>rand(1,6),
                        'living_room'=>rand(1,10),
                        'toilet' =>rand(1,5),
                        'price' => $item['price'],
                        //'average_price' => $item['price'],
                        'acreage'=> $item['area'],
                        'floor' => rand(30,40),
                        'total_floor' => rand(3,20),
                        'orientations' =>rand(0,50),
                        'tags' => '首月免租金,年付减半,押一付一,靠近学校,交通便利',
                        'hits' => 0,
                        'contacts' => $contacts,
                        'looked_num'=>0,
                        'house_type'=>rand(45,48),
                        'renovation'=>rand(41,44),
                        'rent_type'=>rand(49,50),
                        'pay_type'=>rand(50,55),
                        'broker_id' => 0,
                        'user_type' => rand(1,2),
                        'status' => 1,
                        'ordid' => 1000,
                        'create_time' => time(),
                        'update_time' => time(),
                        'ratio'=>0.0,
                        'rec_position'=>rand(0,1),
                        'timeout' => strtotime(date(rand(2022,2030).'-12-31 23:59:59')),
                        'top_time' => 0,
                        'agent_id' => 0
                    ]);
                    DB::table('fang_rental_data')->insert([
                        'house_id' => $rental_id,
                        'file' => '[]',
                        'supporting' => '63,62,61,60,58,57,56,55,54',
                        'seo_title'  =>$item['title']
                    ]);
                    });
                }
                if($i == $num){
                    DB::table('fang_city')->where('id',$city->id)->update(['type'=>2]);
                }

            }
            if($city->id == 500){
                DB::table('fang_city')->update(['type'=>1]);
            }

        }
    }

    /**
     * @param  array  $category
     * @return array
     * @throws GuzzleException
     */
    public function list($category): array
    {
        //var_dump($category);
        //$num = $num==1?:$num.'/';
        $url = "{$category}";
        $puppeteer = new Puppeteer();
        //const puppeteer = require('puppeteer');
        $browser = $puppeteer->launch([
            'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
        ]);
        $page = $browser->newPage();
        $page->goto($url,[
            'timeout' => 10 * 60 * 1000,
            'read_timeout' => 86400,
            'idle_timeout' => 86400,
            'debug' => false
        ]);
        $html = $page->content();
        //var_dump($html);
        $browser->close();
        //$response = $this->client->get($url);//get($url);
        //$content = $response->getBody()->getContents();
        //var_dump($content);
        $crawler = new Crawler();
        $crawler->addHtmlContent($html);
        //$xpath = $this->config['xpath'];
        try {
            return $crawler->filterXPath('//html/body/div[7]/div[3]/div[3]/div[1]/ul/li')->each(function (Crawler $node) use($category) {
                //var_dump($category);
                $title = $node->filter('.text h2 a')->text();
                $url = $node->filter('.text h2 a')->attr('href');
                $jiage = $node->filter('.other .price')->children('span')->text();
                if(strpos($jiage,'-')!==false){
                    $str = explode('-',$jiage);
                    $price = $str[1];
                }elseif ($jiage=='价格待定'){
                    $price = 1000;
                }else{
                    $price = $jiage;
                }
                if($node->filter('.address span')->count()==0){
                    $addr = '';
                }else{
                    $addr1 = $node->filter('.address span')->text();
                    $addr2 = ltrim($addr1,'[');
                    $addr = rtrim($addr2,']');//地址
                }
                if($node->filter('.area a')->count()==0){
                    $area[0] = 0;
                }else{
                    $a = $node->filter('.area a')->text();
                    preg_match( '/\d+/' , $a , $area );//面积：$area[0]
                }

                $img = $node->filter('.img img')->attr('data-src');//图片
//
//
                //var_dump($area[0]);

                return [
                    'title' => $title,
                    //'intro' => $this->replaceIntro($intro),///html/body/div[3]/div[1]/div[2]/ul/li[5]/div[1]/a/img
                    'price' =>$price,
                    'url' => $url,
                    'area' => $area[0],
                    //'id' => $matches['id'],
                    'addr' => $addr,
                    'thumb' => $this->replaceImg($img),
                ];
            });
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }

        return [];
    }

    /**
     * @param  string  $url
     * @return array|false
     * @throws GuzzleException
     */
    public function page($url)
    {
        $title = '';
        $content_url = '';
        $content_title = '';
        //$more = '';
        try {
            //$this->client = new Client(['verify' => false]);
            $response = $this->client->request('GET', $url);
            $content = $response->getBody()->getContents();
            $crawler = new Crawler($content);
            //$content = $crawler->filterXPath($this->config['xpath']['body'])->html();
            ///html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[2]
            //fang_article
            if($crawler->filterXPath('//html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[2]')->count()==1){
                $title = $crawler->filter('.t h5 a')->text();
                $content_url = $crawler->filter('.t h5 a')->attr('href');
                if($crawler->filterXPath('//html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[2]/div[2]/p')->count()==0){
                    $content_title = $crawler->filterXPath('//html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[3]/div[2]/p')->text();
                }else{
                    $content_title = $crawler->filterXPath('//html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[2]/div[2]/p')->text();
                }
            }elseif ($crawler->filterXPath('//html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[3]')->count()==1){
                $title = $crawler->filter('.t h5 a')->text();
                $content_url = $crawler->filter('.t h5 a')->attr('href');
                $content_title = $crawler->filterXPath('//html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[3]/div[2]/p')->text();
            }
            else{
                $title = '';
                $content_url = '';
                $content_title = '';
            }
            //var_dump($content_title);
//            if($crawler->filter('.t h5 a')->count()!=0){
//                $title = $crawler->filter('.t h5 a')->text();
//                $content_url = $crawler->filter('.t h5 a')->attr('href');
//            }else{
//                //$content_title = '';
//                $title = '';
//                $content_url = '';
//            }
//            //$content_title = $crawler->filter('.news .item .text .desc')->children('p')->text();
//            $content_title = $crawler->filterXPath('//html/body/div[7]/div[3]/div[9]/div[1]/div[2]/div[2]/div[2]/p')->text();
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }
        return [
            //'tags' => $matches['tags'],
            'content_title' => $content_title,
            'title' => $title,
            'content_url' => $content_url,
            //'more' => $more
        ];
    }

    //分类
    public function category($url)
    {
        $response = $this->client->get($url);//get($url);
        $content = $response->getBody()->getContents();
        //var_dump($content);
        $crawler = new Crawler($content);
        try{
            $crawler->filterXPath('//*[@id="pro_33"]/dt')->each(function (Crawler $cr) use ($crawler){
                // //*[@id="pro_0"]/dd/a[1]
                $title = $cr->text();
                //$pinyin = new Pinyin();
                //$domain = implode('',$pinyin->convert($title));
                $province = DB::table('fang_province')->insertGetId([
                    'name' => $title,
                    'ordid' => 43,
                    'status' => 1
                ]);
                //var_dump($title);
                //$pid = 1;
//                    $pid = DB::table('fang_city_xin')->insertGetId([
//                        'province_id' => $province,
//                        'name' => $title,
//                        'pid' => 0,
//                        'spid' =>0,
//                        'ordid' => 255,
//                        'status' => 1,
//                        'domain' =>$domain,
//                        'is_hot' => 0,
//                        'lat' => '0',
//                        'lng' => '0'
//                    ]);
                //var_dump(363);
                $crawler->filterXPath('//*[@id="pro_33"]/dd/a')->each(function (Crawler $cr) use ($province){
                    //var_dump($pid);
                    // //*[@id="pro_0"]/dd/a[1]
                    $title = $cr->text();
                    //var_dump($title);
                    $pinyin1 = new Pinyin();
                    $domain1 = implode('',$pinyin1->convert($title));
                    //var_dump($domain1);
                    $href = $cr->attr('href');
                    //var_dump($href);
                    DB::table('fang_city')->insert([
                        'province_id' => $province,
                        'name' => $title,
                        'alias' => $domain1,
                        'pid' => 0,
                        'spid' => '0',
                        'ordid' => 255,
                        'status' => 1,
                        'domain' =>$domain1,
                        'is_hot' => 0,
                        'lat' => '0',
                        'lng' => '0',
                        'url' => 'https:'.$href
                    ]);
                });
            });

        }catch (Exception $e) {
            \Log::error($e->getMessage());
        }

    }
    public function category1($url)
    {
        $response = $this->client->get($url);//get($url);
        $content = $response->getBody()->getContents();
        //var_dump($content);
        $crawler = new Crawler($content);
        try{
            for($i=0;$i<33;$i++){ ////*[@id="pro_1"]/dd
                $crawler->filterXPath('//*[@id="pro_'.$i.'"]/dt')->each(function (Crawler $cr) use ($crawler,$i){
                    // //*[@id="pro_0"]/dd/a[1]
                    $title = $cr->text();
                    $pinyin = new Pinyin();
                    $domain = implode('',$pinyin->convert($title));
                    $province = DB::table('fang_province_copy1')->insertGetId([
                        'name' => $title,
                        'ordid' => $i+10,
                        'status' => 1
                    ]);
                    //var_dump($title);
                    //$pid = 1;
                    $pid = DB::table('fang_city_xin')->insertGetId([
                        'province_id' => $province,
                        'name' => $title,
                        'pid' => 0,
                        'spid' =>0,
                        'ordid' => 255,
                        'status' => 1,
                        'domain' =>$domain,
                        'is_hot' => 0,
                        'lat' => '0',
                        'lng' => '0'
                    ]);
                    //var_dump(363);
                    $crawler->filterXPath('//*[@id="pro_'.$i.'"]/dd/a')->each(function (Crawler $cr) use ($province,$pid){
                        //var_dump($pid);
                        // //*[@id="pro_0"]/dd/a[1]
                        $title1 = $cr->text();
                        $pinyin1 = new Pinyin();
                        $domain1 = implode('',$pinyin1->convert($title1));
                        //var_dump($domain1);
                        $href = $cr->attr('href');
                        //var_dump($href);
                        DB::table('fang_city_xin')->insert([
                            'province_id' => $province,
                            'name' => $title1,
                            'pid' => $pid,
                            'spid' => $pid.'|',
                            'ordid' => 255,
                            'status' => 1,
                            'domain' =>$domain1,
                            'is_hot' => 0,
                            'lat' => '0',
                            'lng' => '0',
                            'url' => 'https:'.$href
                        ]);
                    });
                });
            }

        }catch (Exception $e) {
            \Log::error($e->getMessage());
        }

    }
    public function article_list($url)
    {
        //$this->client = new Client(['verify' => false]);
        $response = $this->client->request('GET', $url);
        $content = $response->getBody()->getContents();
        $crawler = new Crawler($content);
        $article_info = '';

//        if (preg_match('/<meta name="Keywords" content="(?<tags>.*?)"/u', $content, $matches) === false) {
//            return false;
//        }

        try {
            //$content = $crawler->filterXPath($this->config['xpath']['body'])->html();
            //fang_article
            if($crawler->filter('.left')->count()!=0){
                $article_info = $crawler->filter('.left')->html();
            }else{
                $article_info = '';
            }
            //$more = $crawler->filter('.more a')->attr('href');
            //var_dump($addr);
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }
        return [
            //'tags' => $matches['tags'],
            'article_info' => $article_info
        ];
    }
    //楼盘动态的详情
    public function children_page($url)
    {
        $info = '';
        try {
            $response = $this->client->request('GET', $url);
            $content = $response->getBody()->getContents();
            $crawler = new Crawler($content);
            $info = '';

            if (preg_match('/<meta name="Keywords" content="(?<tags>.*?)"/u', $content, $matches) === false) {
                return false;
            }
            //$content = $crawler->filterXPath($this->config['xpath']['body'])->html();
            //fang_article
            if($crawler->filter('.fl')->count() != 0){
                $info = $crawler->filter('.fl')->html();
            }elseif ($crawler->filter('.article')->count()!=0){
                $info = $crawler->filter('.article')->html();
            }elseif ($crawler->filter('.content_text')->count()!=0){
                $info = $crawler->filter('.content_text')->html();
            }
            else{
                $info = '';
            }

            //var_dump($addr);
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }

        return [
            //'tags' => $matches['tags'],
            'info' => $info
        ];
    }
    //更多楼盘信息
    public function news(string $url)
    {
        $response = $this->client->request('GET', $url);
        $content = $response->getBody()->getContents();
        $crawler = new Crawler($content);
        $developer_name = '';


        try {
            return $crawler->filterXPath('//html/body/div[7]/div[2]/div[2]/div[1]/ul[2]/li')->each(function (Crawler $cr) {
                //fang_article
                $new_title = $cr->filter('.tit a')->text();
                $img = $cr->filter('.img img')->attr('src');
                $link = $cr->filter('.tit a')->attr('href');
                $info = $cr->filter('.info')->text();
                //var_dump($new_title);
                return [
                    'new_title' => $new_title,
                    'img' => $img,
                    'link' => $link,
                    'info' => $info
                ];
            });

            //var_dump($addr);
        } catch (Exception $e) {
            \Log::error($e->getMessage());
        }

        return [
            //'tags' => $matches['tags'],
            'developer_name' => $developer_name
        ];
    }

    /**
     * @param  int  $categoryKey
     * @param  array  $post
     * @throws GuzzleException
     */
    public function post(int $categoryKey, $post): void
    {
        /*
        preg_match_all('/<img data-original src="(.*?)"/', $post['content'], $matches);
        foreach ($matches[1] as $item) {
            $post['content'] = str_replace($item, $this->picDown('https://www.qiqu.net'.$item),
                $post['content']);
        }
        $post['content'] = preg_replace('/<a.*?\/a>/', '', $post['content']);
        */
        $resp = $this->client->post($this->config['postUrl'], [
            'form_params' => [
                'enews' => 'AddNews',
                'classid' => $categoryKey,
                'username' => '小编',
                'oldchecked' => '1',
                'title' => $post['title'],
                'ftitle' => '',
                'checked' => '1',
                'newstime' => date('Y-m-d H:i:s'),
                'titlepic' => $post['picUrl'],
                'smalltext' => $post['intro'],
                'dokey' => '1',
                'copyimg' => '0',
                'autosize' => '5000',
                'getfirsttitlepic' => '1',
                'getfirsttitlespicw' => '105',
                'getfirsttitlespich' => '118',
                'onclick' => random_int(1, 1000),
                'infotags' => $post['tags'],
                'addnews' => '提 交',
                'newstext' => $post['content'],
                'writer' => '佚名',
                'keyboard' => $post['tags'],
            ],
        ]);
        if (strpos($resp->getBody()->getContents(), '增加信息成功') === false) {
            echo $resp->getBody()->getContents();
            exit;
        }
    }

    public function putdata(int $categoryKey, $post)
    {

        foreach ($post as $value){
            var_dump($value['title']);
        }
        $res = DB::table('sell_5')->insert([
            'catid'=>114,
            'title' => $post['title'],
            'introduce' => $post['content'],
            'price' => $post['price'],
            'thumb' => $post['thumb'],
            'addr' => $post['addr'],
            'keyword' => $post['keyword'],
        ]);
//        if($res){
//            exit('添加成功');
//        }else{
//            exit('添加失败');
//        }
    }

    /**
     * @param  string  $word  需要替换的字符串
     * @return string
     */
    public function replaceIntro(string $word): string
    {
        $word = str_ireplace('www.QiQu.net ', 'cpadol.com', $word);

        return str_replace('奇趣网', '天下奇闻网', $word);
    }

    public function replacePrice($word)
    {
        preg_match('#([+-]?\d+(\.\d+)?)#',$word,$matches);//提取数字
        return $matches[0];
    }
    public function replaceImg($word)
    {
        if($word=="https://static.loupan.com" || $word=="https://static.loupan.com/no_pic.jpg"){
            $word = "https://static.loupan.com/lpimg/image/2021/0509/1958334076753.jpg?imageView2/3/w/220/h/157/q/75";
        }
        return $word;
    }

    //随机生成n条手机号
    private function randomMobile($n)
    {
        $tel_arr = array(
            '130','131','132','133','134','135','136','137','138','139','144','147','150','151','152','153','155','156','157','158','159','176','177','178','180','181','182','183','184','185','186','187','188','189',
        );
        for($i = 0; $i < $n; $i++) {
            $tmp[] = $tel_arr[array_rand($tel_arr)].mt_rand(1000,9999).mt_rand(1000,9999);
            // $tmp[] = $tel_arr[array_rand($tel_arr)].'xxxx'.mt_rand(1000,9999);
        }
        return array_unique($tmp);
    }
    private function isFenGe(){
        $arr  = array('可分割','不可分割');
        $flipped = array_flip($arr);
        return array_rand($flipped,1);
    }

    public function getXingList()
    {
        $xing = array('赵','钱','孙','李','周','吴','郑','王','冯','陈','褚','卫','蒋','沈','韩','杨',

            '朱','秦','尤','许','何','吕','施','张','孔','曹','严','华','金','魏','陶','姜','戚','谢','邹','喻',

            '柏','水','窦','章','云','苏','潘','葛','奚','范','彭','郎','鲁','韦','昌','马','苗','凤','花','方',

            '任','袁','柳','鲍','史','唐','费','薛','雷','贺','倪','汤','滕','殷','罗','毕','郝','安','常','傅',

            '卞','齐','元','顾','孟','平','黄','穆','萧','尹','姚','邵','湛','汪','祁','毛','狄','米','伏','成',

            '戴','谈','宋','茅','庞','熊','纪','舒','屈','项','祝','董','梁','杜','阮','蓝','闵','季','贾','路',

            '娄','江','童','颜','郭','梅','盛','林','钟','徐','邱','骆','高','夏','蔡','田','樊','胡','凌','霍',

            '虞','万','支','柯','管','卢','莫','柯','房','裘','缪','解','应','宗','丁','宣','邓','单','杭','洪',

            '包','诸','左','石','崔','吉','龚','程','嵇','邢','裴','陆','荣','翁','荀','于','惠','甄','曲','封',

            '储','仲','伊','宁','仇','甘','武','符','刘','景','詹','龙','叶','幸','司','黎','溥','印','怀','蒲',

            '邰','从','索','赖','卓','屠','池','乔','胥','闻','莘','党','翟','谭','贡','劳','逄','姬','申','扶',

            '堵','冉','宰','雍','桑','寿','通','燕','浦','尚','农','温','别','庄','晏','柴','瞿','阎','连','习',

            '容','向','古','易','廖','庾','终','步','都','耿','满','弘','匡','国','文','寇','广','禄','阙','东',

            '欧','利','师','巩','聂','关','荆','司马','上官','欧阳','夏侯','诸葛','闻人','东方','赫连','皇甫',

            '尉迟','公羊','澹台','公冶','宗政','濮阳','淳于','单于','太叔','申屠','公孙','仲孙','轩辕','令狐',

            '徐离','宇文','长孙','慕容','司徒','司空');
        $flipped = array_flip($xing);
        return array_rand($flipped,1);
    }
    public function getMingList()

    {

        $ming=array('伟','刚','勇','毅','俊','峰','强','军','平','保','东','文','辉','力','明','永',

            '健','世','广','志','义','兴','良','海','山','仁','波','宁','贵','福','生','龙','元','全','国','胜',

            '学','祥','才','发','武','新','利','清','飞','彬','富','顺','信','子','杰','涛','昌','成','康','星',

            '光','天','达','安','岩','中','茂','进','林','有','坚','和','彪','博','诚','先','敬','震','振','壮',

            '会','思','群','豪','心','邦','承','乐','绍','功','松','善','厚','庆','磊','民','友','裕','河','哲',

            '江','超','浩','亮','政','谦','亨','奇','固','之','轮','翰','朗','伯','宏','言','若','鸣','朋','斌',

            '梁','栋','维','启','克','伦','翔','旭','鹏','泽','晨','辰','士','以','建','家','致','树','炎','德',

            '行','时','泰','盛','雄','琛','钧','冠','策','腾','楠','榕','风','航','弘','秀','娟','英','华','慧',

            '巧','美','娜','静','淑','惠','珠','翠','雅','芝','玉','萍','红','娥','玲','芬','芳','燕','彩','春',

            '菊','兰','凤','洁','梅','琳','素','云','莲','真','环','雪','荣','爱','妹','霞','香','月','莺','媛',

            '艳','瑞','凡','佳','嘉','琼','勤','珍','贞','莉','桂','娣','叶','璧','璐','娅','琦','晶','妍','茜',

            '秋','珊','莎','锦','黛','青','倩','婷','姣','婉','娴','瑾','颖','露','瑶','怡','婵','雁','蓓','纨',

            '仪','荷','丹','蓉','眉','君','琴','蕊','薇','菁','梦','岚','苑','婕','馨','瑗','琰','韵','融','园',

            '艺','咏','卿','聪','澜','纯','毓','悦','昭','冰','爽','琬','茗','羽','希','欣','飘','育','滢','馥',

            '筠','柔','竹','霭','凝','晓','欢','霄','枫','芸','菲','寒','伊','亚','宜','可','姬','舒','影','荔',

            '枝','丽','阳','妮','宝','贝','初','程','梵','罡','恒','鸿','桦','骅','剑','娇','纪','宽','苛','灵',

            '玛','媚','琪','晴','容','睿','烁','堂','唯','威','韦','雯','苇','萱','阅','彦','宇','雨','洋','忠',

            '宗','曼','紫','逸','贤','蝶','菡','绿','蓝','儿','翠','烟');
        $flipped = array_flip($ming);
        return array_rand($flipped,1);
    }

    /**
     * @param  string  $url
     * @return string
     * @throws Exception
     */
    public function picDown(string $url): string
    {
        $timePath = date('Ymd');
        $picName = $this->randStr(11);

        if (preg_match('/^.*\.(?<extension>.*?)$/', $url, $matches) === false) {
            exit('图片扩展名获取失败');
        }

        $path = $timePath.'/'.$picName.'.'.$matches['extension'];

        Storage::put($path, file_get_contents($url));

        return config('app.url').config('spider.picPath').'/'.$path;
    }

    /**
     * @param  int  $length
     * @return string
     * @throws Exception
     */
    public function randStr(int $length): string
    {
        $str = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $len = strlen($str) - 1;
        $randStr = '';
        for ($i = 0; $i < $length; $i++) {
            $num = random_int(0, $len);
            $randStr .= $str[$num];
        }

        return $randStr;
    }
}
