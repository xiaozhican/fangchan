<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class ExternalLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ExternalLink';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client([
            'timeout' => 10,
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $urlPrefix = 'http://www.2134.com.cn/seo/data.php?p=';
        $urlSuffix = '&dn=minwan.com';

        $currPage = 1;

        do {
            $response = $this->client->get($urlPrefix.$currPage.$urlSuffix);
            $content = $response->getBody()->getContents();

            $maxPage = 176;

            if (preg_match_all('/href="(https?:\/\/.*?)"/s', $content, $matches) === false) {
                exit('没有找到');
            }

            echo implode(PHP_EOL, $matches[1]);
            echo PHP_EOL;

            $currPage++;
        } while ($currPage < $maxPage);
    }

    public function zhipaiwu()
    {
        $urlPrefix = 'http://www.zhipaiwu.com/index.php/Out_links/data.html?p=';
        $urlSuffix = '&dn=miwan.com';

        $currPage = 1;

        do {
            $response = $this->client->get($urlPrefix.$currPage.$urlSuffix);
            $content = $response->getBody()->getContents();

            $maxPage = 256;

            if (preg_match_all('/href="(https?:\/\/.*?)"/s', $content, $matches) === false) {
                exit('没有找到');
            }

            echo implode(PHP_EOL, $matches[1]);
            $currPage++;
        } while ($currPage < $maxPage);
    }

    public function iseeyu()
    {
        $urlPrefix = 'https://www.iseeyu.com/tool/data.php?p=';
        $urlSuffix = '&dn=minwan.com';

        $currPage = 1;

        do {
            $response = $this->client->get($urlPrefix.$currPage.$urlSuffix);
            $content = $response->getBody()->getContents();

            if (($currPage === 1) && preg_match('/（\d+\/(\d+)）/u', $content, $matches) === false) {
                exit('没有获取到总页面数量');
            }

            $maxPage = $matches[1];

            if (preg_match_all('/href="(http.*)/', $content, $matches) === false) {
                exit('没有找到');
            }

            echo implode(PHP_EOL, $matches[1]);
            echo PHP_EOL;

            $currPage++;
        } while ($currPage < $maxPage);
    }

    public function seowhy()
    {
        $urlPrefix = 'https://didi.seowhy.com/wailian.html?page=';
        $urlSuffix = '&url=minwan.com';

        $currPage = 1;

        do {
            $response = $this->client->get($urlPrefix.$currPage.$urlSuffix, [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]);
            $content = $response->getBody()->getContents();

            $resp = json_decode($content, true);

            if ($resp['code'] !== 0) {
                exit('API 出错');
            }

            $maxPage = ceil($resp['count'] / 10);

            foreach ($resp['data'] as $item) {
                echo $item['url'].PHP_EOL;
            }

            $currPage++;
        } while ($currPage < $maxPage);
    }

    public function cjzzc()
    {
        $url = 'http://www.cjzzc.com/wailian.html?page=';

        $currPage = 1;
        $offset = 0;

        do {
            $response = $this->client->post($url.$currPage, [
                'form_params' => [
                    'url' => 'minwan.com',
                    'token_cjzzc' => '08d26ad6320a6f15a5501518cbf99770',
                    'page' => $offset,
                ],
            ]);
            $content = $response->getBody()->getContents();

            if (($currPage === 1) && preg_match('/第\d+\/(\d+)页/u', $content, $matches) === false) {
                exit('没有获取到总页面数量');
            }

            $maxPage = $matches[1];

            $crawler = new Crawler($content);
            $crawler->filterXPath("//table[@id='list-table']//tr[@class='active']")->each(function (Crawler $node) {
                $url = $node->filterXPath('//td[2]')->text();
                echo $url.PHP_EOL;
            });

            $offset += 10;
            $currPage++;
        } while ($currPage < $maxPage);
    }
}
