<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $table = 'sell_5';

    public $timestamps = false;
}
